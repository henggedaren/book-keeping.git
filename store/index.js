import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let t = new Date().getTime();

const store = new Vuex.Store({
	state: {
		// 用户id
		openid: null,
		// token 有些接口可以为空
		token : null,
		// 版本号
		version: '1.0.0',
		// 分享标题
		shareData : [
			{title:"推荐最好用的记账软件给你!",path:"",imageUrl:""},
			{title:"跟我一起记账吧!",path:"",imageUrl:""},
			{title:"这个记账软件超好用!",path:"",imageUrl:""},
		]
	},
	mutations: {
		login(state, provider) {
			state.hasLogin = true;
		},
		logout(state) {
			state.hasLogin = false
			state.openid = null;
			state.token = null;
			state.billId = 0;
		},
		setOpenid(state, openid) {
			state.openid = openid
		}
	}
})

export default store