# 记记账

#### 介绍
基于Thinkphp6.0 + uniapp开发的记账系统, 快速实现各种小程序+APP端

#### 软件架构
PHP + mysql + apache

#### 线上地址

![输入图片说明](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-d169d58c-574d-44df-94f3-68676168a987/b2b37104-f136-46ac-a1bc-783773d5ef21.png "QQ截图20210318115313.png")

#### 展示


![输入图片说明](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-d169d58c-574d-44df-94f3-68676168a987/97542393-7b30-47ed-ab91-18cb1a88c306.png "1.png")

![输入图片说明](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-d169d58c-574d-44df-94f3-68676168a987/e2ac1c1c-5d67-432c-8d6e-fd7c4aa6fb00.png "2.png")

![输入图片说明](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-d169d58c-574d-44df-94f3-68676168a987/b905a7d3-5f1c-4103-b15e-b274123724c3.png "3.png")

![输入图片说明](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-d169d58c-574d-44df-94f3-68676168a987/f3e0f0de-e4e1-48d4-8f69-28de1a4fecbe.png "4.png")

![输入图片说明](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-d169d58c-574d-44df-94f3-68676168a987/d59c3468-4b5b-4ac1-b947-17362e4c420e.png "5.png")

![输入图片说明](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-d169d58c-574d-44df-94f3-68676168a987/10b5473f-e4a7-4afd-984b-51d82b327eec.png "6.png")